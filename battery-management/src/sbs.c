/**
 * \file
 *
 * \brief Smart Battery System driver
 *
 * This file defines a useful set of functions for the SBS
 *
 * Copyright (c) 2015 Toby Churchill Ltd.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <asf.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "sbs.h"
#include "sbs_adc.h"
#include "sbs_mm.h"

/* Battery Status bits */
#define NOT_PRESENT		0
#define DISCHARGING		1
#define FULLY_CHARGED		2
#define FULLY_DISCHARGED	3
#define CHARGING		4

struct bat_status_info {
	uint16_t status;
	char *desc;
};

struct bat_status_info bat_status_array[] = {
	{0x0080, "NOT_PRESENT"},
	{0x0040, "DISCHARGING"},
	{0x0020, "FULLY_CHARGED"},
	{0x0010, "FULLY_DISCHARGED"},
	{0x0000, "CHARGING"},
};

/* Measured actual battery capacity */
static int32_t actual_capacity;

/* Tracks current capacity in coulombs */
static int32_t current_capacity;

/* Percentage state of charge */
int8_t pct_soc;

/* Amount of measurments that were in the fully charged window */
int8_t fully_charged_measurements = 0;

/*
 * Discharge Accumulation
 *
 * We need to count charge cycles, but we don't want to count every partial
 * discharge and subsequent charge as a "charge cycle". So we keep track of
 * the coulombs that are discharged and increment the number of cycles when
 * this value reaches the battery capacity.
 */
static int32_t coulombs_discharged;

/**
 * \brief Update the voltage value in memory map.
 */
void update_voltage_now(void)
{
	uint16_t voltage;

	voltage = adc_get_voltage_now();

	mm_write(SBS_VOLTAGE_NOW, &voltage, sizeof(uint16_t));
}

/**
 * \brief Update the current value in memory map.
 */
void update_current_now(void)
{
	int16_t current;

	current = adc_get_current_now();

	mm_write(SBS_CURRENT_NOW, &current, sizeof(int16_t));
}

/**
 * \brief Write the firmware version to the memory map.
 */
void update_firmware_version(void)
{
	uint8_t version;

	version = (uint8_t)_VERSION_;

	mm_write(SBS_FIRMWARE_VERSION, &version, sizeof(uint8_t));
}

/**
 * \brief Returns the voltage value measurement.
 */
uint16_t read_voltage_now(void)
{
	uint16_t voltage;

	mm_read(SBS_VOLTAGE_NOW, &voltage, sizeof(uint16_t));
	
	return voltage;
}

/**
 * \brief Returns the current value measurement.
 */
int16_t read_current_now(void)
{
	uint16_t current;

	mm_read(SBS_CURRENT_NOW, &current, sizeof(int16_t));

	return (int16_t)current;
}

/**
 * \brief Returns true when battery is fully charged.
 * 
 * The PMIC (TPS65217) charges in 3 stages once a battery is detected:
 *
 * - Precharge: A current of ~10% of the fast charge current is applied. If
 *		the voltage climbs to 2.9v (default) in under and hour, it is
 *		considered safe to move to fast charge.
 * - Fast Charge: Fast charge current is applied until the voltage rises to
 *		  the charging voltage, at which point charger moves on to
 *		  constant voltage charging.
 * - Constant Voltage: Charging continues at the charging voltage until the
 *		       current drops below (by default) 7.5% of the charging
 *		       voltage.
 *
 * At this point the charger stops if termination isn't disabled. Charging will
 * restart if the voltage drops by ~100mV.
 *
 * To detect the end of charging, we will look for the battery voltage to be
 * above 95% of the nominal voltage and the current to drop to under 10% of the
 * fast charge current, while still being above 25mV to differentiate from pmic
 * off.
 */
static bool is_fully_charged(int8_t status, int16_t current)
{
/* Small measurements are prone to error, so ignore them for the purpose of
 * changing status
 */
#define ERROR_MARGIN 25
	uint16_t voltage;
	int16_t charging_current;
	int32_t vterm, iterm;

	mm_read(SBS_DESIGN_VOLTAGE, &voltage, sizeof(uint16_t));
	vterm = voltage * 95L / 100L;

	mm_read(SBS_FAST_CHARGING_CURRENT, &charging_current, sizeof(int16_t));
	iterm = charging_current / 10L;

	voltage = read_voltage_now();

	if (status == CHARGING) {
		/* We will drop into FULLY_CHARGED state a bit early as we won't be
		 * putting significant power in past iterm and it ensures we don't
		 * spuriously think we are fully charged before the PMIC has started
		 * at boot.
		 *
		 * Also require the system to be in the fully charged window for more then
		 * 5 measurements (thus more then 5 seconds)
		 */
		if ((voltage > vterm) && (current < -ERROR_MARGIN && current > -iterm)) {
				if (fully_charged_measurements > 5) {
					fully_charged_measurements = 0;
					return true;
				}
				fully_charged_measurements++;
				return false;
			}
	} else if (status == FULLY_CHARGED) {
		/* Only drop out of fully charged status if the system discharges at more
		 * than iterm, which basically means we are drawing more than 50mA with
		 * current values.)
		 */
		if (current > iterm)
			return false;

		return true;
	}

	return false;
}

static bool is_present(void)
{
	uint16_t voltage = read_voltage_now();
	int16_t current = read_current_now();

	/* Currently we believe the battery to be missing if voltage is
	 * greater than 4.3V or less than 2.8v. Current must also be below
	 * +/-10mA
	 */
	pr_debug("INFO: Battery Presence: ");

	if ((voltage > 2800) && (voltage < 4300)) {
		pr_debug("Voltage in range (%d)\r\n", voltage);
		return true;
	}

	if ((current < -15) || (current > 15)) {
		pr_debug("Current in range (%d)\r\n", current);
		return true;
	}

	pr_debug("No battery\r\n");

	return false;
}

int8_t get_battery_status(void)
{
	uint16_t temp;
	int8_t i;

	mm_read(SBS_BATTERY_STATUS, &temp, sizeof(uint16_t));

	/* Map between values stored in memory map and array index */
	for (i = 0; i < sizeof(bat_status_array); i++) {
		if(temp == bat_status_array[i].status){
			return i;
		}
	}

	return -1;
}

void init_battery_status(void)
{
	uint16_t stored_ah;

	mm_read(SBS_ACTUAL_CAPACITY, &stored_ah, sizeof(stored_ah));
	actual_capacity = stored_ah * 3600L;

	mm_read(SBS_REM_CAPACITY, &stored_ah, sizeof(stored_ah));
	current_capacity = stored_ah * 3600L;

	pct_soc = ((uint32_t)current_capacity * 100L) / actual_capacity;
	mm_write(SBS_STATE_OF_CHARGE, &pct_soc, sizeof(pct_soc));

	mm_read(SBS_DISCHARGE_COULOMBS, &coulombs_discharged,
		sizeof(coulombs_discharged));
}

/**
 * \brief Update the Smart Battery's status word which contains Status bit flags.
 */
void update_battery_status(void)
{
	int8_t status, status_old;
	uint16_t stored_ah;
	int16_t current = read_current_now();
	int8_t pct_soc_old;

	pct_soc_old = pct_soc;
	status_old = get_battery_status();

	if (! is_present()) {
		/*
		 * Other than flagging battery seems to be missing, don't
		 * change the data. The user can switch the battery off and
		 * we don't want this detected as a fully discharged battery.
		 */
		status = NOT_PRESENT;

	} else if (is_fully_charged(status_old, current)) {
		status = FULLY_CHARGED;
		pct_soc = 100;
		current_capacity = actual_capacity;
	} else if (current > 0) {
		int32_t rate;
		status = DISCHARGING;

		/* HACK: Fudge discharge rate. Increase by 15% */
		rate = (current * 115L) / 100L;

		current_capacity -= rate;
		coulombs_discharged += rate;

		/* We can't go under 0 capacity */
		if (current_capacity < 0)
			current_capacity = 0;

		pct_soc = ((uint32_t)current_capacity * 100L) / actual_capacity;

	} else {
		status = CHARGING;

		current_capacity -= current;	/* negative number */

		/* We can't go over actual_capacity */
		if (current_capacity > actual_capacity)
			current_capacity = actual_capacity;

		pct_soc = ((uint32_t)current_capacity * 100L) / actual_capacity;

		/*
		 * Do not show 100% until we are in FULLY_CHARGED
		 * state.
		 */
		if (pct_soc > 99)
			pct_soc = 99;
	}

	pr_debug("INFO: Full Capacity: %ld\r\n", actual_capacity);
	pr_debug("INFO: Current Capacity: %ld\r\n", current_capacity);
	pr_debug("INFO: State of Charge: %d\r\n", pct_soc);
	pr_debug("INFO: Discharged Coulombs: %ld\r\n", coulombs_discharged);

	/* 
	 * Update remaining capacity and discharge capacity when percentage
	 * increases.
	 */
	if(pct_soc != pct_soc_old) {
		mm_write(SBS_STATE_OF_CHARGE, &pct_soc, sizeof(pct_soc));
		/* Convert from coulombs to Ampere hours */
		stored_ah = current_capacity / 3600L;
		mm_write(SBS_REM_CAPACITY, &stored_ah, sizeof(stored_ah));

		mm_write(SBS_DISCHARGE_COULOMBS, &coulombs_discharged,
			 sizeof(coulombs_discharged));
	}

	/*
	 * Calculate expected capacity.
	 *
	 * We keep track of the coulombs discharged. When this value reaches
	 * the current expected battery capacity the cycle_count is increased
	 * and 1/400 of the design capacity is removed from the batteries
	 * reported capacity.
	 */
	if (coulombs_discharged > actual_capacity) {
		uint32_t design_capacity;
		uint16_t cycle_count;

		/* Reduce coulombs discharged */
		coulombs_discharged -= actual_capacity;

		/* Increment cycle_count */
		mm_read(SBS_CYCLE_COUNT, &cycle_count,
			sizeof(cycle_count));
		cycle_count = cycle_count + 1;
		mm_write(SBS_CYCLE_COUNT, &cycle_count,
			 sizeof(cycle_count));

		/* Update actual capacity */
		pr_debug("INFO: Actual capacity was: %ld\r\n", actual_capacity);
		mm_read(SBS_PACK_CAPACITY, &stored_ah, sizeof(stored_ah));
		design_capacity = stored_ah * 3600L;

		actual_capacity = actual_capacity - (design_capacity / 400);

		stored_ah = actual_capacity / 3600L;
		mm_write(SBS_ACTUAL_CAPACITY, &stored_ah, sizeof(stored_ah));

		pr_debug("INFO: Actual capacity now: %ld\r\n", actual_capacity);
	}

	/*
	 * Update the status if changed.
	 */
	if (status_old != status) {
		mm_write(SBS_BATTERY_STATUS, &(bat_status_array[status].status),
			 sizeof(uint16_t));

		pr_debug("INFO: %s -> %s\r\n",
			 bat_status_array[status_old].desc,
			 bat_status_array[status].desc);
	} else {
		pr_debug("INFO: %s\r\n", bat_status_array[status_old].desc);
	}
}

/**
 * \brief Update memory map
 */
void update_memory_map(void)
{
	update_voltage_now();
	update_current_now();
	update_battery_status();

#ifdef DEBUG
  {
		uint16_t voltage;
		int16_t current;
		int8_t status = get_battery_status();

		mm_read(SBS_CURRENT_NOW, &current, sizeof(int16_t));
		mm_read(SBS_VOLTAGE_NOW, &voltage, sizeof(int16_t));

		printf("STAT: %ld %d %ld %d %d %d\r\n", rtc_get_time(), pct_soc, current_capacity, voltage, current, status);
  }
#endif
}
