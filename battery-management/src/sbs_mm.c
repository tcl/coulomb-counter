/**
 * \file
 *
 * \brief Smart Battery System driver - Memory Map Management
 *
 * This file defines a useful set of functions for the memory management map interface
 *
 * Copyright (c) 2015 Toby Churchill Ltd.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <asf.h>
#include <string.h>
#include "sbs_mm.h"
#include "errno.h"

/* Structure to hold data stored in EEPROM. */
/* Note this is already 16 bytes, thus one full write for the EEPROM,
 * When more data is added this should be split up  */
struct nvm_data {
    uint8_t counter;
    uint8_t firmware_version;
    uint16_t cycle_count;
    uint16_t rem_capacity;
    uint16_t actual_capacity;
    uint32_t discharge_coulombs;
    uint16_t full_capacity;
    uint16_t chksum;
};

/**
 * \brief Smart Battery Memory Map
 *
 *  +---------------+ 0x00
 *  |      OTP      |
 *  |---------------| 0x50
 *  |      NVM      |
 *  |---------------| 0x70
 *  |      RAM      |
 *  +---------------+ 0x80
 *
 *	0x0a - 0x0b : Charging voltage - 4200 mV (0x1068)
 *	0x0c - 0x0d : Design voltage - 3700 mV (0xE74)
 *	0x0e - 0x0f : Fast-Charging Current - 700 mA (0x2BC)
 *	0x10 - 0x11 : Max T, Low T - 60 degree / 0 degree
 *	0x12 - 0x13 : Pack Capacity  - 6900 mAh (0x1AF4)
 *	0x18 - 0x19 : Serial number
 *	0x20 - 0x2f : Manufacturer name
 *	0x30 - 0x3f : Model name
 *	0x40 - 0x44 : Device Chemistry
 *
 *	0x70 - 0x71 : Voltage now
 *	0x72 - 0x73 : Current now
 *	0x74 - 0x75 : Battery status
 *  0x76        : State of charge
 *  0x77 - 0x78 : Cycle count
 *  0x7d        : Firmware version
 *  0x7e - 0x7f : Data Checksum
 */
volatile uint8_t memory_map[SBS_MEMORY_MAP_SIZE] = {
/*			 00    01    02    03    04    05    06    07    08    09    0a    0b    0c    0d    0e    0f */
/* 0000 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68, 0x10, 0x74, 0x0e, 0xbc, 0x02,
/* 0010 */	 60 , 0x00, 0xf4, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0020 */	 'T',  'o',  'b',  'y',  ' ',  'C',  'h',  'u',  'r',  'c',  'h',  'i',  'l',  'l', 0x00, 0x00,
/* 0030 */	 '1',  '8',  '6',  '5',  '5',  ' ',  '1',  's',  '2',  'p', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0040 */	 'L',  'I',  'O',  'N', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0050 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0060 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0070 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static uint8_t memory_map_default[SBS_MEMORY_MAP_SIZE] = {
/*			 00    01    02    03    04    05    06    07    08    09    0a    0b    0c    0d    0e    0f */
/* 0000 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68, 0x10, 0x74, 0x0e, 0xbc, 0x02,
/* 0010 */	 60 , 0x00, 0xf4, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0020 */	 'T',  'o',  'b',  'y',  ' ',  'C',  'h',  'u',  'r',  'c',  'h',  'i',  'l',  'l', 0x00, 0x00,
/* 0030 */	 '1',  '8',  '6',  '5',  '5',  ' ',  '1',  's',  '2',  'p', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0040 */	 'L',  'I',  'O',  'N', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0050 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0060 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 0070 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

volatile bool reset_after_battery_replacement = false;

struct nvm_data persistent_data;
int8_t persistent_data_offset;

uint16_t calc_chksum(uint8_t *, uint8_t);
void update_persist_chksum(struct nvm_data *);
int8_t store_data(int8_t, void *, uint8_t);
int8_t recall_data(void);

void twi_init(void);

uint16_t calc_chksum(uint8_t *address, uint8_t len)
{
	uint16_t chksum = 0;
	uint8_t i;

	for (i = 0; i < len; i++) {
		chksum +=  address[i];
	}

	return chksum;
}

void update_persist_chksum(struct nvm_data *data)
{
	uint8_t *data_array = (uint8_t *)data;

	data->chksum = calc_chksum(data_array, sizeof(*data) - 2);
}

/*
 * When the EEPROM is busy writing it will return NACK when a write command is
 * sent. Implement a function to wait for it to send ACK and therefore be free.
 */
void at24_ack_poll(void)
{
	enum status_code ret = ERR_BUSY;

	/* Write data block in next location */
	twi_package_t packet_write = {
		.addr         = {0, 0, 0},
		.addr_length  = 1,
		.chip         = CONF_SBS_I2C_EEPROM_ADDR,
		.buffer       = 0,
		.length       = 0,
		.no_wait      = false
	};

	while (ret != STATUS_OK) {
		ret = twi_master_write(&TWIC, &packet_write);
	}
}

#define AT24_EEPROM_ADDR_PAGES 4
#define AT24_EEPROM_ADDR_PAGE_SIZE 256
#define AT24_EEPROM_PAGES 64
#define AT24_EEPROM_RESERVE_PAGES 4
#define AT24_EEPROM_PAGE_SIZE 16

int8_t store_data(int8_t offset, void *buf, uint8_t len)
{
	enum status_code ret;
	uint16_t data_address;
	uint8_t page, page_offset;
	
	/* Ensure chksum is correct */
	update_persist_chksum(buf);

	/* Calculate address page and page offset */
	data_address = (offset * AT24_EEPROM_PAGE_SIZE);
	page_offset = data_address % AT24_EEPROM_ADDR_PAGE_SIZE;
	page = data_address / AT24_EEPROM_ADDR_PAGE_SIZE;

	/* Write data block in next location */
	twi_package_t packet_write = {
		.addr         = {page_offset, 0, 0 },
		.addr_length  = 1,
		.chip         = CONF_SBS_I2C_EEPROM_ADDR | page,
		.buffer       = buf,
		.length       = len,
		.no_wait      = false
	};

	ret = twi_master_write(&TWIC, &packet_write);
	if (ret != STATUS_OK) {
		printf("ERROR: Failed to write EEPROM");
		return -1;
	}

	at24_ack_poll();

	return 0;
}

int8_t recall_data(void)
{
	struct nvm_data data_block[2];
	enum status_code ret;
	uint8_t i;
	uint16_t chksum;

	uint8_t count = 0;
	uint8_t current = 0;
	uint8_t prev = 0;
	uint8_t copy = 0;

	int8_t offset = -1;

	uint16_t data_address;
	uint8_t page, page_offset;

	for (i = AT24_EEPROM_RESERVE_PAGES; i < AT24_EEPROM_PAGES; i++)
	{
		pr_debug("DEBUG: Looking at EEPROM page %d\r\n", i);

		prev = current;
		current = count % 2;

		/* Calculate address page and page offset */
		data_address = (i * AT24_EEPROM_PAGE_SIZE);
		page_offset = data_address % AT24_EEPROM_ADDR_PAGE_SIZE;
		page = data_address / AT24_EEPROM_ADDR_PAGE_SIZE;

		/* Read block */
		twi_package_t packet_read = {
			.addr         = { page_offset, 0, 0 },
			.addr_length  = 1,
			.chip         = CONF_SBS_I2C_EEPROM_ADDR | page,
			.buffer       = &data_block[current],
			.length       = sizeof(persistent_data),
			.no_wait      = false
		};

		ret = twi_master_read(&TWIC, &packet_read);
		if (ret != STATUS_OK){
			printf("ERROR: Failed to read EEPROM\r\n");
			return -1;
		}

		/* Check CRC */
		chksum = calc_chksum((uint8_t *)&data_block[current], sizeof(struct nvm_data) - 2);

		if (chksum != data_block[current].chksum) {
			printf("ERROR: Invalid chksum in EEPROM in location %d: 0x%x != 0x%x\r\n",
			       i, chksum, data_block[current].chksum);
			continue;
		}

		count++;

		/* We need to read two to compare */
		if (count == 1) {
			/* Store in case we only have 1 which is valid */
			offset = i;
			continue;
		}

		if ((uint8_t)(data_block[current].counter - data_block[prev].counter) > AT24_EEPROM_PAGES)
			break;

		offset = i;
		copy = current;

	}

	/* Found none */
	if (count == 0)
		return -1;

	memcpy((void *)(&persistent_data), (void *)&data_block[copy], sizeof(struct nvm_data));

	return offset;
}

/**
 * \brief Write buffer to memory map
 *
 * \param address   the address to where to write
 * \param buf       pointer to the data
 * \param len       the number of bytes to write
 */
int8_t mm_write(uint8_t address, const void* buf, uint8_t len)
{
	uint8_t i;
	uint16_t csum = 0;


	if (address + len > SBS_MEMORY_MAP_CSUM_MSB_bp) {
		printf("ERROR: Attempted to write beyond memory map end\r\n");
		return -1;
	}

	/* Write block into memory_map */
	memcpy((void *)(&memory_map[address]), buf, len);

	/* Calc memory_map checksum */
	for (i = 0; i < SBS_MEMORY_MAP_CSUM_MSB_bp; i++) {
		csum +=  memory_map[i];
	}

	/* Store the checksum for data integrity */
	memory_map[SBS_MEMORY_MAP_CSUM_MSB_bp] = MSB(csum);
	memory_map[SBS_MEMORY_MAP_CSUM_LSB_bp] = LSB(csum);


	/* Store current persistent data checksum */
	csum = persistent_data.chksum;

	/* Copy any new data into persistent data structure */
	persistent_data.firmware_version = memory_map[SBS_FIRMWARE_VERSION];
	memcpy(&persistent_data.cycle_count,
	       (void *)(&memory_map[SBS_CYCLE_COUNT]),
	       sizeof(persistent_data.cycle_count));
	memcpy(&persistent_data.rem_capacity,
	       (void *)(&memory_map[SBS_REM_CAPACITY]),
	       sizeof(persistent_data.rem_capacity));
	memcpy(&persistent_data.actual_capacity,
	       (void *)(&memory_map[SBS_ACTUAL_CAPACITY]),
	       sizeof(persistent_data.actual_capacity));
	memcpy(&persistent_data.full_capacity,
	       (void *)(&memory_map[SBS_PACK_CAPACITY]),
	       sizeof(persistent_data.full_capacity));
	memcpy(&persistent_data.discharge_coulombs,
	       (void *)(&memory_map[SBS_DISCHARGE_COULOMBS]),
	       sizeof(persistent_data.discharge_coulombs));

	update_persist_chksum(&persistent_data);

	/* If CRC hasn't changed we're done. */
	if (csum == persistent_data.chksum)
		return 0;

	persistent_data.counter++;

	/* chksum updated in store_data() */
	//update_persist_chksum(&persistent_data);

	store_data(persistent_data_offset, (void *)&persistent_data,
		   sizeof(persistent_data));

	persistent_data_offset++;
	if (persistent_data_offset == AT24_EEPROM_PAGES)
		persistent_data_offset = AT24_EEPROM_RESERVE_PAGES;


	return 0;
}

/**
 * \brief Read buffer from memory map
 *
 * \param address   the address to where to read
 * \param buf       pointer to the data
 * \param len       the number of bytes to read
 */
void mm_read(uint8_t address, void* buf, uint8_t len)
{
	memcpy(buf, (void *)(&memory_map[address]), len);
}

/** 
 * \brief Erase all the memory map
 */
void mm_erase_all(void)
{
	uint8_t addr, value = 0xff;
	
	printf("Erasing all memory map ... ");
	for (addr = 0; addr < SBS_MEMORY_MAP_SIZE; addr++)
		mm_write(addr, &value, 1);
	printf("done\r\n");
}

/** 
 * \brief Erase the EEPROM
 */
void mm_erase_eeprom(void)
{
	uint16_t data_address;
	uint8_t page, page_offset;
	int8_t i;
	enum status_code ret;

	int8_t buf[AT24_EEPROM_PAGE_SIZE];

	printf("Erasing EEPROM ... ");

	for (i = 0; i < AT24_EEPROM_PAGE_SIZE; i++)
		buf[i] = 0xff;

	for (i = 0; i < AT24_EEPROM_PAGES; i++)
	{
		/* Calculate address page and page offset */
		data_address = (i * AT24_EEPROM_PAGE_SIZE);
		page_offset = data_address % AT24_EEPROM_ADDR_PAGE_SIZE;
		page = data_address / AT24_EEPROM_ADDR_PAGE_SIZE;

		/* Write data block in next location */
		twi_package_t packet_write = {
			.addr         = {page_offset, 0, 0 },
			.addr_length  = 1,
			.chip         = CONF_SBS_I2C_EEPROM_ADDR | page,
			.buffer       = buf,
			.length       = AT24_EEPROM_PAGE_SIZE,
			.no_wait      = false
		};

		ret = twi_master_write(&TWIC, &packet_write);
		if (ret != STATUS_OK) {
			printf("ERROR: Failed to write EEPROM");
			return;
		}

		at24_ack_poll();

	}

	printf("done\r\n");
}

/**
 * \brief Program default values.
 */
void mm_init(void)
{
	uint16_t capacity;
	int8_t current_offset;

	twi_init();

	/* Write default values */
	printf("Programming default values ... ");
	memcpy((void *)memory_map, memory_map_default, SBS_MEMORY_MAP_SIZE);
	printf("done\r\n");

	/*
	 * Check to see if we have valid data in EEPROM. If we have, update
	 * the defaults. Function returns offset used, save this for future
	 * updates.
	 */
	current_offset = recall_data();
	if (current_offset == -1) {

		printf("No data, building default settings\r\n");

		/*
		 * Didn't find persistent data - set defaults based on default
		 * capacity.
		 */
		mm_read(SBS_PACK_CAPACITY, &capacity, sizeof(capacity));
		pr_debug("INFO: Design Capacity: %d\r\n", capacity);

		/*
		 * We aren't tracking capacity quite accurately enough yet. We
		 * discharge before getting to ~0%. Intentionally reduce the
		 * working capacity of the battery by 25% to ensure we don't
		 * run out of juice before getting to ~0% on the users display.
		 */
		pr_debug("INFO: ***HACK*** reducing capacity by 25%%\r\n");
		capacity = capacity * 0.75;

		pr_debug("INFO: Full Capacity: %d\r\n", capacity);
		memcpy((void *)(&memory_map[SBS_ACTUAL_CAPACITY]), &capacity,
		       sizeof(capacity));
		persistent_data_offset = AT24_EEPROM_RESERVE_PAGES;
	} else {
		/*
		 * NOTE: If struct nvm_data changes in a later FW revision,
		 *	 here's the place to check for the firmware version
		 *	 stored in the struct and do the right thing...
		 */
		memcpy((void *)(&memory_map[SBS_CYCLE_COUNT]),
		       &persistent_data.cycle_count,
		       sizeof(persistent_data.cycle_count));
		memcpy((void *)(&memory_map[SBS_REM_CAPACITY]),
		       &persistent_data.rem_capacity,
		       sizeof(persistent_data.rem_capacity));
		memcpy((void *)(&memory_map[SBS_ACTUAL_CAPACITY]),
		       &persistent_data.actual_capacity,
		       sizeof(persistent_data.actual_capacity));
		memcpy((void *)(&memory_map[SBS_PACK_CAPACITY]),
		       &persistent_data.full_capacity,
		       sizeof(persistent_data.full_capacity));
		memcpy((void *)(&memory_map[SBS_DISCHARGE_COULOMBS]),
		       &persistent_data.discharge_coulombs,
		       sizeof(persistent_data.discharge_coulombs));

		persistent_data_offset = current_offset + 1;
		if(persistent_data_offset >= AT24_EEPROM_PAGES)
			persistent_data_offset = AT24_EEPROM_RESERVE_PAGES;
	}
}

/**
 * \brief Dump memory map
 */
void mm_dump(void)
{
	uint8_t i, value;

	printf("Memory Map:\r\n");
	for (i = 0; i < SBS_MEMORY_MAP_SIZE; i++) {
		mm_read(i, &value, 1);
		printf("{ 0x%02x, 0x%02x }, ", i, value);
	}
	printf("\r\n");
}

void mm_store(void)
{
	persistent_data.counter++;

	persistent_data_offset++;
	if(persistent_data_offset == AT24_EEPROM_PAGES)
		persistent_data_offset = AT24_EEPROM_RESERVE_PAGES;

	store_data(persistent_data_offset, (void *)&persistent_data,
			  sizeof(persistent_data));
}

static void nvm_at24_twi_init(void)
{
	twi_master_options_t opt = {
		.speed = CONF_SBS_I2C_EEPROM_SPEED,
		.chip  = CONF_SBS_I2C_EEPROM_ADDR,
	};

	twi_master_setup(&TWIC, &opt);
}

void twi_init(void)
{
	/* Disable AT24 EEPROM write protect */
	ioport_set_pin_level(CONF_PIN_AT24_WP, 0);

	nvm_at24_twi_init();
}
